# Jeu de stratégie
> Authors: 👷‍♀️Lenoir Juliette & 👷Moriceau Axel

## Run the game
> make all

This will generate the *.class files and the JavaDoc for the project and run the game.
The *.class will be in out/
The documentation will be in documentation/

> make clean

Will delete the output and the documentation

> make cleanrun

Will run everything like `make all` and then will run `make clean`

> make compile

Will create the *.class in out/

> make run

Will run what is in the out/ directory

> make doc

Will create the documentation in documentation/