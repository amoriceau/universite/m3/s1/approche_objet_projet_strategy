import application.services.GameService;
import domain.aggregate.City;
import domain.aggregate.Game;
import domain.entity.players.Player;
import infrastructure.repository.GameRepository;
import interfaces.GameCLI;
import shared.utils.ANSICodes;

import java.util.Scanner;

/**
 * The type Main.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Game game = new Game();
        GameRepository repository = new GameRepository();

        String input;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("How many players ?\n");
            input = scanner.nextLine();

            try {
                int number = Integer.parseInt(input);
                if (number<10) {
                    for (int i = 0; i < number; i++) {
                        Player p = new Player("P" + (i + 1));
                        p.setCity(new City());
                        game.addPlayer(p);
                    }
                    break;
                }else {
                    System.out.println("There are too much players. (Max: 10)");
                }


            } catch (NumberFormatException e) {
                System.out.println("The number of players you entered is invalid.");
            }
        }

        GameService gameService = new GameService(repository, game);
        GameCLI client = new GameCLI(gameService);

        client.startGameSession();

        while (!game.isFinished()) {
            client.processPlayerTurn();
        }

        System.out.println(ANSICodes.CYAN + "Game has ended." + ANSICodes.RESET);
        // System.out.println(game.getHistory());
    }
}