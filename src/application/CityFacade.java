package application;

import domain.aggregate.City;
import domain.entity.buildings.Builder;
import domain.entity.buildings.Building;
import domain.entity.buildings.BuildingDirector;
import domain.entity.buildings.Office;
import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;
import shared.utils.ANSICodes;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The type City facade.
 */
public class CityFacade {

    private static Builder instanciateBuilder(City city,BuildingType type){
        Builder builder;
        BuildingDirector director = city.getBuildingRepository().director;
        switch (type){
            case WOODEN_CABIN -> builder = director.constructWoodenCabin();
            case HOUSE -> builder = director.constructHouse();
            case APARTMENT -> builder =  director.constructAppartement();
            case FARM -> builder =  director.constructFarm();
            case QUARRY -> builder = director.constructQuarry();
            case LUMBER_MILL -> builder = director.constructLumberMill();
            case CEMENT_PLANT -> builder = director.constructCementPlant();
            case STEEL_MILL -> builder = director.constructSteelMill();
            case TOOL_FACTORY -> builder = director.constructToolFactory();

            default -> {
                System.out.println(type + " is not an acceptable building type");
                throw new RuntimeException("Invalid building type");
            }
        }
        return builder;
    }

    /**
     * Create building building.
     *
     * @param city the city
     * @param type the type
     * @return the building
     */
    public static Building createBuilding(City city, BuildingType type){
        Builder builder = CityFacade.instanciateBuilder(city, type);
        boolean cityHasResources = city.hasResources(builder.getBaseCost());

        if(!cityHasResources){
            System.out.println("The city does not have the resources to build a new " + type);
            return null;
        }

        for (ResourceEntry entry: builder.getBaseCost()) {
            city.consumeResource(entry.getResource(), entry.getQuantity());
        }

        Building building = builder.build();
        city.getBuildingRepository().save(building);

        CityFacade.bindLivingPlaces(city);
        CityFacade.bindWorkingPlaces(city);

        return building;
    }

    /**
     * Create free building building.
     *
     * @param city the city
     * @param type the type
     * @return the building
     */
    public static Building createFreeBuilding(City city, BuildingType type){
        Builder builder = CityFacade.instanciateBuilder(city, type);
        Building building = builder.build();
        building.setConstructionState(building.getConstructionTime());
        city.getBuildingRepository().save(building);
        return building;
    }

    /**
     * Get building cost list.
     *
     * @param city         the city
     * @param buildingType the building type
     * @return the list
     */
    public static List<ResourceEntry> getBuildingCost(City city, BuildingType buildingType){
        Builder builder = CityFacade.instanciateBuilder(city, buildingType);
        return builder.getBaseCost();
    }

    /**
     * Get building cost at level list.
     *
     * @param city  the city
     * @param type  the type
     * @param level the level
     * @return the list
     */
    public static List<ResourceEntry> getBuildingCostAtLevel(City city, BuildingType type, BuildingLevel level){
        Builder builder = CityFacade.instanciateBuilder(city, type);

        return builder.withLevel(level).build().getBaseCost();
    }

    /**
     * Bind living places.
     *
     * @param city the city
     */
    public static void bindLivingPlaces(City city) {
        List<Building> livingPlaces = city.getBuildingRepository().findFinishedByBuildingType(BuildingType.HOUSE, BuildingType.APARTMENT, BuildingType.FARM, BuildingType.QUARRY, BuildingType.WOODEN_CABIN);
        for (Building livingPlace: livingPlaces) {
            if (livingPlace.isFullOfInhabitants()){
                continue;
            }
            for (Inhabitant homeless: city.getInhabitantRepository().findAllWithoutLivingPlace()) {
                if (livingPlace.isFullOfInhabitants()){
                    break;
                }
                livingPlace.addInhabitant(homeless);
                homeless.setLivingPlace(livingPlace);
            }
        }
    }

    /**
     * Bind working places.
     *
     * @param city the city
     */
    public static void bindWorkingPlaces(City city) {
        List<Office> workingPlaces = city.getBuildingRepository().findAllFinishedOffice();
        Collections.sort(workingPlaces, Comparator.comparingInt(place -> place.getWorkers().size()));

        for (Office workingPlace: workingPlaces) {
            if (workingPlace.isFullOfWorkers()){
                continue;
            }
            for (Worker notWorking: city.getInhabitantRepository().findAllWithoutWorkingPlace()) {
                if (workingPlace.isFullOfWorkers()){
                    break;
                }

                workingPlace.addWorker(notWorking);
                notWorking.setWorkingPlace(workingPlace);

            }
        }
    }

    /**
     * Upgrade building.
     *
     * @param city   the city
     * @param target the target
     */
    public static void upgradeBuilding(City city, Building target) {
        if(target.getLevel().equals(BuildingLevel.LEVEL_V)){
            System.out.println(ANSICodes.YELLOW + "The building " + target.identity() + " is already at maximum level");
        }

        target.upgrade();
        if(!city.hasResources(target.getBaseCost())){
            System.out.println(ANSICodes.RED + "You don't have enough resources to upgrade "+ target.identity() + ANSICodes.RESET);
            System.out.println("Needed resources: " + target.getBaseCost());
            System.out.println("Actual resources: " + city.getResources());
            target.downgrade();
        }
        for (ResourceEntry entry: target.getBaseCost()) {
            city.consumeResource(entry.getResource(), entry.getQuantity());
        }
        System.out.println(ANSICodes.CYAN + target.identity() + " is now level " + target.getLevel() + ANSICodes.RESET);
    }

    /**
     * Destroy building.
     *
     * @param city   the city
     * @param target the target
     */
    public static void destroyBuilding(City city, Building target){
        for (Inhabitant inhabitant: target.getInhabitants()) {
            inhabitant.setLivingPlace(null);
            target.getInhabitants().remove(inhabitant.getUuid());
            inhabitant.setHappiness(Math.max(inhabitant.getHappiness() - 2, 0));
        }

        if (target instanceof Office){
            for (Worker worker: ((Office) target).getWorkers()) {
                worker.setWorkingPlace(null);
                ((Office) target).getWorkers().remove(worker.getUuid());
                worker.setHappiness(Math.max(worker.getHappiness() - 1, 0));

            }
        }

        city.getBuildingRepository().delete(target.getUuid());
    }

    public static void removeInhabitant(City city, Inhabitant inhabitant) {
        if(inhabitant.getLivingPlace() != null){
            inhabitant.getLivingPlace().getInhabitants().remove(inhabitant.getUuid());
            inhabitant.setLivingPlace(null);
        }

        if (inhabitant instanceof Worker){
            if(((Worker) inhabitant).getWorkingPlace() != null){
                ((Worker) inhabitant).getWorkingPlace().getWorkers().remove(inhabitant.getUuid());
                ((Worker) inhabitant).setWorkingPlace(null);
            }
        }

        city.getInhabitantRepository().delete(inhabitant.getUuid());
    }
}
