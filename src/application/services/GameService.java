package application.services;

import application.CityFacade;
import domain.aggregate.Game;
import domain.entity.buildings.Building;
import domain.vo.Action;
import infrastructure.repository.GameRepository;
import shared.enums.BuildingType;
import shared.utils.ANSICodes;

/**
 * The type Game service.
 */
public class GameService {
    private final GameRepository gameRepository;
    private Game game;

    /**
     * Instantiates a new Game service.
     *
     * @param gameRepository the game repository
     */
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    /**
     * Instantiates a new Game service.
     *
     * @param gameRepository the game repository
     * @param game           the game
     */
    public GameService(GameRepository gameRepository, Game game) {
        this.gameRepository = gameRepository;
        this.game = game;
    }

    /**
     * Perform player action.
     *
     * @param action the action
     */
    public void performPlayerAction(Action action) {
        switch (action.playerAction()){
            case BUILD -> {
                CityFacade.createBuilding(this.game.getCurrentPlayer().getCity(), (BuildingType) action.target());
            }
            case DESTROY -> {
                CityFacade.destroyBuilding(this.game.getCurrentPlayer().getCity(), (Building) action.target());
            }
            case UPGRADE -> {
                CityFacade.upgradeBuilding(this.game.getCurrentPlayer().getCity(), (Building) action.target());
            }
            default -> {
                System.out.println("Action not supported: " + ANSICodes.RED + action.playerAction() + ANSICodes.RESET);
                return;
            }
        }
        this.game.getCurrentTurn().actions().add(action);
    }


    /**
     * Start game.
     */
    public void startGame() {
        game.start();
        CityFacade.bindLivingPlaces(game.getCurrentPlayer().getCity());
        CityFacade.bindWorkingPlaces(game.getCurrentPlayer().getCity());
    }

    /**
     * Save game.
     */
    public void saveGame(){
        this.gameRepository.save(game);
    }

    /**
     * Current player wants to stop.
     */
    public void currentPlayerWantsToStop() {
        this.game.getCurrentPlayer().setWantsToStop(true);
    }

    /**
     * Bind.
     *
     * @param game the game
     */
    public void bind(Game game) {
        this.game = game;
    }

    /**
     * Handle current player turn.
     */
    public void handleCurrentPlayerTurn() {
        this.game.takeTurn();
    }

    /**
     * Instance game.
     *
     * @return the game
     */
    public Game instance(){
        return this.game;
    }
}
