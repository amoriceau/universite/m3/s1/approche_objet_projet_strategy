package domain.aggregate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import application.CityFacade;
import domain.entity.buildings.Housing;
import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.vo.resources.Food;
import domain.vo.resources.Gold;
import domain.vo.resources.Stone;
import domain.vo.resources.Wood;
import shared.abstracts.Unique;
import domain.entity.resources.ResourceEntry;
import domain.vo.Resource;
import infrastructure.repository.BuildingRepository;
import infrastructure.repository.InhabitantRepository;
import shared.enums.BuildingType;
import shared.enums.InhabitantSex;
import shared.utils.Random;

/**
 * The type City.
 */
public class City extends Unique {
    /**
     * The Building repository.
     */
    protected BuildingRepository buildingRepository;
    /**
     * The Inhabitant repository.
     */
    protected InhabitantRepository inhabitantRepository;
    /**
     * The Resources.
     */
    protected Map<String, ResourceEntry> resources;

    /**
     * Instantiates a new City.
     *
     * @param buildingRepository   the building repository
     * @param inhabitantRepository the inhabitant repository
     */
    public City(BuildingRepository buildingRepository, InhabitantRepository inhabitantRepository) {
        this.buildingRepository = buildingRepository;
        this.inhabitantRepository = inhabitantRepository;
    }

    /**
     * Instantiates a new City.
     *
     * @param buildingRepository   the building repository
     * @param inhabitantRepository the inhabitant repository
     * @param resources            the resources
     */
    public City(BuildingRepository buildingRepository, InhabitantRepository inhabitantRepository, Map<String, ResourceEntry> resources) {
        this(buildingRepository, inhabitantRepository);
        this.resources = resources;
    }

    /**
     * Instantiates a new City.
     */
    public City(){
        this.buildingRepository = new BuildingRepository();
        this.inhabitantRepository = new InhabitantRepository();
        this.resources = new HashMap<>();
        this.init();

    }

    private void init(){
        // *** City base resources *** //
        this.addResources(
                new ResourceEntry(new Gold(), 15),
                new ResourceEntry(new Wood(), 50),
                new ResourceEntry(new Stone(), 25),
                new ResourceEntry(new Food(), 30)
        );

        // *** City base inhabitants *** //
        int minAge = 12;
        int maxAge = 70;
        int peoples = Random.between(2,5);
        for (int i = 0; i < peoples; i++) {
            int age = Random.between(minAge, maxAge);
            InhabitantSex sex = Random.value() >= 0.5 ? InhabitantSex.M : InhabitantSex.F;
            this.inhabitantRepository.save(new Inhabitant(age,sex));
        }

        // *** City base inhabitants *** //
        int workingForce = Random.between(3,6);
        minAge = 18;
        maxAge = 67;
        for (int i = 0; i < workingForce; i++) {
            int age = Random.between(minAge, maxAge);
            InhabitantSex sex = Random.value() >= 0.5 ? InhabitantSex.M : InhabitantSex.F;
            this.inhabitantRepository.save(new Worker(age,sex));
        }

        // *** City base buildings *** //
        CityFacade.createFreeBuilding(this, BuildingType.HOUSE);
        CityFacade.createFreeBuilding(this, BuildingType.HOUSE);
        CityFacade.createFreeBuilding(this, BuildingType.QUARRY);
        CityFacade.createFreeBuilding(this, BuildingType.FARM);

        CityFacade.bindLivingPlaces(this);
        CityFacade.bindWorkingPlaces(this);

    }


    /**
     * Gets building repository.
     *
     * @return the building repository
     */
    public BuildingRepository getBuildingRepository() {
        return buildingRepository;
    }

    /**
     * Sets buildings.
     *
     * @param repository the repository
     */
    public void setBuildings(BuildingRepository repository) {
        this.buildingRepository = repository;
    }

    /**
     * Gets inhabitant repository.
     *
     * @return the inhabitant repository
     */
    public InhabitantRepository getInhabitantRepository() {
        return inhabitantRepository;
    }

    /**
     * Sets inhabitant repository.
     *
     * @param repository the repository
     */
    public void setInhabitantRepository(InhabitantRepository repository) {
        this.inhabitantRepository = repository;
    }

    /**
     * Gets resources.
     *
     * @return the resources
     */
    public Map<String, ResourceEntry> getResources() {
        return this.resources;
    }

    /**
     * Add resources.
     *
     * @param resources the resources
     */
    public void addResources(ResourceEntry ...resources) {
        for (ResourceEntry resource: resources) {
            List<ResourceEntry> resourceEntry = new ArrayList<>();
            resourceEntry.add(resource);
            if (this.hasResources(false,resourceEntry)){
                ResourceEntry existingEntry = this.resources.get(resource.getResource().getName());
                existingEntry.setQuantity(existingEntry.getQuantity() + resource.getQuantity());
            }else {
                this.resources.put(resource.getResource().getName(), resource);
            }
        }
    }

    /**
     * Consume resource.
     *
     * @param resource the resource
     * @param quantity the quantity
     */
    public void consumeResource(Resource resource, int quantity){
        ResourceEntry cityResource = this.resources.get(resource.getName());
        if (cityResource == null){
            System.out.println("The resource " + resource.getName() + " does not exists in this city");
            return;
        }

        if (cityResource.getQuantity() < quantity){
            System.out.println("Not enough " + resource.getName() + ". (" + cityResource.getQuantity()+" remaining while " + quantity + " were needed).");
            return;
        }

        cityResource.setQuantity(cityResource.getQuantity() - quantity);
    }

    /**
     * Has resources boolean.
     *
     * @param verbose   the verbose
     * @param resources the resources
     * @return the boolean
     */
    public boolean hasResources(boolean verbose, List<ResourceEntry> resources){
        for (ResourceEntry resource: resources) {
            ResourceEntry cityResource = this.resources.get(resource.getResource().getName());

            if(cityResource == null){
                if(verbose) System.out.println("City does not have " + resource.getResource().getName() + " in the available resources.");
                return false;
            }

            if (resource.getQuantity() > cityResource.getQuantity()){
                if(verbose) System.out.println("City has not enough " + resource.getResource().getName() + " (" + resource.getQuantity() + " needed, but only " + cityResource.getQuantity() + " available).");
                return false;
            }
        }

        return true;
    }

    /**
     * Has resources boolean.
     *
     * @param resources the resources
     * @return the boolean
     */
    public boolean hasResources(List<ResourceEntry> resources){
        return this.hasResources(true, resources);
    }

    /**
     * Sets resources.
     *
     * @param resources the resources
     */
    public void setResources(Map<String, ResourceEntry> resources) {
        this.resources = resources;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("City{");
        sb.append("buildingRepository=").append(buildingRepository);
        sb.append(", inhabitantRepository=").append(inhabitantRepository);
        sb.append(", resources=").append(resources);
        sb.append('}');
        return sb.toString();
    }
}
