package domain.aggregate;

import application.CityFacade;
import domain.entity.buildings.Building;
import domain.entity.buildings.Office;
import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.players.Player;
import domain.entity.resources.ResourceEntry;
import domain.vo.Turn;
import domain.vo.resources.Food;
import shared.abstracts.Unique;
import shared.enums.GameState;
import shared.enums.InhabitantSex;
import shared.utils.ANSICodes;
import shared.utils.Random;

import java.util.*;

/**
 * The type Game.
 */
public class Game extends Unique {
    private List<Player> players;
    private int currentPlayer;
    private GameState state;
    private List<Turn> history;
    private Turn currentTurn;

    /**
     * Instantiates a new Game.
     */
    public Game() {
        this.state = GameState.INITIALIZING;
        this.players = new ArrayList<>();
        this.history = new ArrayList<>();
        this.currentPlayer = 0;
    }

    /**
     * Add player.
     *
     * @param player the player
     */
    public void addPlayer(Player player) {
        players.add(player);
    }

    /**
     * Start.
     */
    public void start() {
        if (players.size() < 1) {
            throw new IllegalStateException("Not enough players to start the game.");
        }
        this.initTurn();
        this.state = GameState.RUNNING;
    }


    /**
     * Take turn.
     */
    public void takeTurn() {
        if (this.state != GameState.RUNNING) {
            throw new IllegalStateException("Game is not in progress.");
        }
        this.getCurrentPlayer().increaseTurnCount();

        this.push(this.currentTurn);
        this.processTurn();

        if (this.checkForGameEnd()) {
            this.state = GameState.FINISHED;
            System.out.println(ANSICodes.PURPLE + this.getCurrentPlayer().getName() + ANSICodes.RESET + " wants to stop the game!");
            return;
        }

        this.currentPlayer = (this.currentPlayer + 1) % players.size();
        this.initTurn();
    }

    private void processTurn() {
        Player currentPlayer = players.get(this.currentPlayer);
        City city = currentPlayer.getCity();

        // *** Consume 1 food per inhabitant *** //
        for (Inhabitant inhabitant : city.getInhabitantRepository().findAll()) {
            List<ResourceEntry> resource = new ArrayList<>();
            resource.add(new ResourceEntry(new Food(), 1));
            if (city.hasResources(resource)) {
                city.consumeResource(new Food(), 1);
            } else {
                inhabitant.setHappiness(Math.max(inhabitant.getHappiness() - 1, 0));
                if (inhabitant.getHappiness() == 0) {
                    System.out.println(ANSICodes.BOLD + ANSICodes.ITALIC + inhabitant.getUuid() + ANSICodes.RESET + " has left the city."+ ANSICodes.RED +"(Cause lack of Food)" + ANSICodes.RESET);
                    CityFacade.removeInhabitant(this.getCurrentPlayer().getCity(), inhabitant);

                }
            }
        }

        // *** Building advancement *** //
        for (Building building : city.getBuildingRepository().findAllUnfinished()) {
            building.setConstructionState(building.getConstructionState() + 1);
        }

        // *** Building resources consumption/production *** //
        boolean cityHasResources;
        Set<UUID> couldNotConsume = new HashSet<>();

        // *** Consumption *** //
        for (Office building : city.getBuildingRepository().findAllFinishedOffice()) {
            cityHasResources = city.hasResources(building.getConsumption());

            if (!cityHasResources) {
                couldNotConsume.add(building.getUuid());
            } else {
                for (ResourceEntry resource : building.getConsumption()) {
                    city.consumeResource(resource.getResource(), resource.getQuantity());
                }
            }
        }


        // *** Production *** //
        for (Office building : city.getBuildingRepository().findAllFinishedOffice()) {
            if (!couldNotConsume.contains(building.getUuid()) && !building.getWorkers().isEmpty()) {
                for (ResourceEntry resource : building.getProduction()) {
                    ResourceEntry resourceEntryRatio = new ResourceEntry(resource.getResource(), resource.getQuantity());
                    city.addResources(resourceEntryRatio);
                }
            }
        }

        // *** Managing Inhabitants *** //
        int dead = 0;
        for (Inhabitant inhabitant : city.getInhabitantRepository().findAll()) {
            if (inhabitant.getLivingPlace() == null) {
                inhabitant.setHappiness(Math.min(inhabitant.getHappiness() - 1, 0));
                if (inhabitant.getHappiness() == 0) {
                    System.out.println(ANSICodes.BOLD + ANSICodes.ITALIC + inhabitant.getUuid() + ANSICodes.RESET + " has left the city." + ANSICodes.RED + "(Cause Homeless)" + ANSICodes.RESET);
                    CityFacade.removeInhabitant(this.getCurrentPlayer().getCity(), inhabitant);
                }
            }

            inhabitant.setAge(inhabitant.getAge() + 1);

            if (!(inhabitant instanceof Worker) && inhabitant.getAge() >= 20 && inhabitant.getAge() <= 67) {
                System.out.println(ANSICodes.CYAN + "Promote " + inhabitant.getUuid());
                Worker nw = inhabitant.promote();
                city.getInhabitantRepository().delete(inhabitant.getUuid());
                city.getInhabitantRepository().save(nw);
                System.out.println("new uuid: " + nw.getUuid() + ANSICodes.RESET);
            }

            if (inhabitant.getAge() > 67 && inhabitant instanceof Worker) {
                System.out.println(ANSICodes.CYAN +"Demote " + inhabitant.getUuid());
                Inhabitant ni = ((Worker) inhabitant).demote();
                city.getInhabitantRepository().delete(inhabitant.getUuid());
                city.getInhabitantRepository().save(ni);
                System.out.println("new uuid: " + ni.getUuid() + ANSICodes.RESET);

            }


            boolean dies;
            if (inhabitant.getAge() >= 80) {
                int age = inhabitant.getAge();
                double chanceOfLiving;
                if (age < 99) {
                    chanceOfLiving = 0.20 - 0.01 * (age - 80);
                } else {
                    chanceOfLiving = 0.01;
                }
                dies = Random.value() > chanceOfLiving;
            } else {
                dies = Random.value() > 0.999;
            }

            if (dies) {
                dead++;
                System.out.println(ANSICodes.RED + inhabitant.getUuid() + " is dead at age " + inhabitant.getAge() + "." + ANSICodes.RESET);
                CityFacade.removeInhabitant(this.getCurrentPlayer().getCity(), inhabitant);
            }
        }

        // *** New inhabitants *** //
        int minAge = 1;
        int maxAge = 50;
        int availableSpaceInCity = 0;

        for (Building building : city.getBuildingRepository().findAllFinishedAndNotFull()) {
            availableSpaceInCity += building.getMaxInhabitants() - building.getInhabitants().size();
        }

        int peoples = Random.between(0, availableSpaceInCity + Math.max((availableSpaceInCity / 20), 1));

        for (int i = 0; i < peoples; i++) {
            int age = Random.between(minAge, maxAge);
            InhabitantSex sex = Random.value() >= 0.5 ? InhabitantSex.M : InhabitantSex.F;
            city.inhabitantRepository.save(new Inhabitant(age, sex));
        }

        CityFacade.bindLivingPlaces(city);
        CityFacade.bindWorkingPlaces(city);

        System.out.println("City of " + currentPlayer.getName() + " grows. " + peoples + " arrived in the city while " + dead + " died...");
    }

    /**
     * Gets current player.
     *
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return this.players.get(this.currentPlayer);
    }

    /**
     * Gets current turn.
     *
     * @return the current turn
     */
    public Turn getCurrentTurn() {
        return this.currentTurn;
    }

    private boolean checkForGameEnd() {
        for (Player player : this.players) {
            if (player.wantsToStop()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished() {
        return this.state.equals(GameState.FINISHED);
    }

    private void initTurn() {
        this.currentTurn = new Turn(this.getCurrentPlayer(), new ArrayList<>());
    }

    /**
     * Push.
     *
     * @param turn the turn
     */
    public void push(Turn turn) {
        this.history.add(turn);
    }

    /**
     * Gets history.
     *
     * @return the history
     */
    public List<Turn> getHistory() {
        return history;
    }

    /**
     * Gets players.
     *
     * @return the players
     */
    public List<Player> getPlayers() {
        return this.players;
    }
}
