package domain.entity.buildings;

import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;

import java.util.List;
import java.util.Set;

/**
 * The interface Builder.
 */
public interface Builder {
    /**
     * With max inhabitants builder.
     *
     * @param maxInhabitants the max inhabitants
     * @return the builder
     */
    Builder withMaxInhabitants(int maxInhabitants);

    /**
     * With max workers builder.
     *
     * @param maxWorkers the max workers
     * @return the builder
     */
    Builder withMaxWorkers(int maxWorkers);

    /**
     * With construction time builder.
     *
     * @param constructionTime the construction time
     * @return the builder
     */
    Builder withConstructionTime(int constructionTime);

    /**
     * With construction state builder.
     *
     * @param constructionState the construction state
     * @return the builder
     */
    Builder withConstructionState(int constructionState);

    /**
     * With inhabitants builder.
     *
     * @param inhabitants the inhabitants
     * @return the builder
     */
    Builder withInhabitants(Set<Inhabitant> inhabitants);

    /**
     * With workers builder.
     *
     * @param workers the workers
     * @return the builder
     */
    Builder withWorkers(Set<Worker> workers);

    /**
     * Gets base cost.
     *
     * @return the base cost
     */
    List<ResourceEntry> getBaseCost();

    /**
     * With base cost builder.
     *
     * @param baseCost the base cost
     * @return the builder
     */
    Builder withBaseCost(List<ResourceEntry> baseCost);

    /**
     * With production builder.
     *
     * @param production the production
     * @return the builder
     */
    Builder withProduction(List<ResourceEntry> production);

    /**
     * With consumption builder.
     *
     * @param consumption the consumption
     * @return the builder
     */
    Builder withConsumption(List<ResourceEntry> consumption);

    /**
     * With level builder.
     *
     * @param level the level
     * @return the builder
     */
    Builder withLevel(BuildingLevel level);

    /**
     * With type builder.
     *
     * @param type the type
     * @return the builder
     */
    Builder withType(BuildingType type);

    /**
     * Build building.
     *
     * @return the building
     */
    Building build();
}
