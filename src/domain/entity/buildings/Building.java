package domain.entity.buildings;

import domain.entity.inhabitants.Inhabitant;
import shared.abstracts.Unique;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;
import shared.utils.ANSICodes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The type Building.
 */
public abstract class Building extends Unique {
    /**
     * The Level.
     */
    BuildingLevel level;
    /**
     * The Type.
     */
    BuildingType type;
    /**
     * The Base cost.
     */
    List<ResourceEntry> baseCost;
    /**
     * The Construction time.
     */
    int constructionTime;
    /**
     * The Construction state.
     */
    int constructionState;

    /**
     * The Inhabitants.
     */
    Set<Inhabitant> inhabitants;

    /**
     * The Max inhabitants.
     */
    int maxInhabitants;


    /**
     * Instantiates a new Building.
     *
     * @param level             the level
     * @param type              the type
     * @param baseCost          the base cost
     * @param constructionTime  the construction time
     * @param constructionState the construction state
     * @param maxInhabitants    the max inhabitants
     * @param inhabitants       the inhabitants
     */
    public Building(BuildingLevel level, BuildingType type, List<ResourceEntry> baseCost, int constructionTime, int constructionState, int maxInhabitants, Set<Inhabitant> inhabitants) {
        this.level = level;
        this.baseCost = baseCost;
        this.constructionTime = constructionTime;
        this.constructionState = constructionState;
        this.type = type;
        this.maxInhabitants = maxInhabitants;

        if(this.inhabitants != null){
            this.inhabitants = inhabitants;
        }else{
            this.inhabitants = new HashSet<>();
        }
    }

    /**
     * Get type building type.
     *
     * @return the building type
     */
    public BuildingType getType(){
        return this.type;
    }

    /**
     * Get construction state int.
     *
     * @return the int
     */
    public int getConstructionState(){
        return this.constructionState;
    }

    /**
     * Get level building level.
     *
     * @return the building level
     */
    public BuildingLevel getLevel(){
        return this.level;
    }

    /**
     * Sets level.
     *
     * @param level the level
     */
    public void setLevel(BuildingLevel level) {
        this.level = level;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(BuildingType type) {
        this.type = type;
    }

    /**
     * Gets base cost.
     *
     * @return the base cost
     */
    public List<ResourceEntry> getBaseCost() {
        List<ResourceEntry> computedCost = new ArrayList<>();
        for (ResourceEntry resource: baseCost) {
            computedCost.add(new ResourceEntry(resource.getResource(), resource.getQuantity() * level.getValue()));
        }
        return computedCost;
    }

    /**
     * Sets base cost.
     *
     * @param baseCost the base cost
     */
    public void setBaseCost(List<ResourceEntry> baseCost) {
        this.baseCost = baseCost;
    }

    /**
     * Gets construction time.
     *
     * @return the construction time
     */
    public int getConstructionTime() {
        return constructionTime;
    }

    /**
     * Sets construction time.
     *
     * @param constructionTime the construction time
     */
    public void setConstructionTime(int constructionTime) {
        this.constructionTime = constructionTime;
    }

    /**
     * Sets construction state.
     *
     * @param constructionState the construction state
     */
    public void setConstructionState(int constructionState) {
        this.constructionState = constructionState;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished(){
        return this.constructionState == this.constructionTime;
    }

    /**
     * Is full of inhabitants boolean.
     *
     * @return the boolean
     */
    public boolean isFullOfInhabitants(){
        return  this.inhabitants.size() == this.getMaxInhabitants();
    }

    /**
     * Gets inhabitants.
     *
     * @return the inhabitants
     */
    public Set<Inhabitant> getInhabitants() {
        return inhabitants;
    }

    /**
     * Sets inhabitants.
     *
     * @param inhabitants the inhabitants
     */
    public void setInhabitants(Set<Inhabitant> inhabitants) {
        this.inhabitants = inhabitants;
    }

    /**
     * Gets max inhabitants.
     *
     * @return the max inhabitants
     */
    public int getMaxInhabitants() {
        return maxInhabitants + (int)Math.floor(level.getValue() * (maxInhabitants/5));
    }

    /**
     * Sets max inhabitants.
     *
     * @param maxInhabitants the max inhabitants
     */
    public void setMaxInhabitants(int maxInhabitants) {
        this.maxInhabitants = maxInhabitants;
    }

    /**
     * Add inhabitant.
     *
     * @param inhabitant the inhabitant
     */
    public void addInhabitant(Inhabitant inhabitant) {
        if(isFullOfInhabitants()) return;
        this.inhabitants.add(inhabitant);
    }


    /**
     * Upgrade.
     */
    public void upgrade(){
        switch (this.level){
            case LEVEL_I -> this.level = BuildingLevel.LEVEL_II;
            case LEVEL_II -> this.level = BuildingLevel.LEVEL_III;
            case LEVEL_III -> this.level = BuildingLevel.LEVEL_IV;
            case LEVEL_IV -> this.level = BuildingLevel.LEVEL_V;
            default -> System.out.println(ANSICodes.RED + "Invalid level attribution" + ANSICodes.RESET);
        }
    }

    /**
     * Downgrade.
     */
    public void downgrade(){
        switch (this.level){
            case LEVEL_II -> this.level = BuildingLevel.LEVEL_I;
            case LEVEL_III -> this.level = BuildingLevel.LEVEL_II;
            case LEVEL_IV -> this.level = BuildingLevel.LEVEL_III;
            case LEVEL_V -> this.level = BuildingLevel.LEVEL_IV;
            default -> System.out.println(ANSICodes.RED + "Invalid level attribution" + ANSICodes.RESET);
        }
    }

    /**
     * Identity string.
     *
     * @return the string
     */
    public String identity(){
        return this.getType()+"@"+this.hashCode();
    }

    /**
     * Gets next level.
     *
     * @return the next level
     */
    public BuildingLevel getNextLevel() {
        return  switch (this.level){
            case LEVEL_I -> BuildingLevel.LEVEL_II;
            case LEVEL_II -> BuildingLevel.LEVEL_III;
            case LEVEL_III -> BuildingLevel.LEVEL_IV;
            default -> BuildingLevel.LEVEL_V;
        };
    }
}
