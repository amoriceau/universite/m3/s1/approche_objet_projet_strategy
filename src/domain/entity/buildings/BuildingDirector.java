package domain.entity.buildings;

import domain.entity.resources.ResourceEntry;
import domain.vo.resources.*;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Building director.
 */
public class BuildingDirector {

    /**
     * Construct wooden cabin builder.
     *
     * @return the builder
     */
    public Builder constructWoodenCabin(){
        int maxInhabitants = 2;
        int maxWorkers = 2;
        int constructionTime = 2;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.WOODEN_CABIN;


        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 1));
        cost.add(new ResourceEntry(new Wood(), 1));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Wood(), 2));
        production.add(new ResourceEntry(new Food(), 2));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct house builder.
     *
     * @return the builder
     */
    public Builder constructHouse(){
        int maxInhabitants = 4;
        int constructionTime = 4;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.HOUSE;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 1));
        cost.add(new ResourceEntry(new Wood(), 2));
        cost.add(new ResourceEntry(new Stone(), 2));


        return new HousingBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withBaseCost(cost)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct appartement builder.
     *
     * @return the builder
     */
    public Builder constructAppartement(){
        int maxInhabitants = 60;
        int constructionTime = 6;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.APARTMENT;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 4));
        cost.add(new ResourceEntry(new Wood(), 50));
        cost.add(new ResourceEntry(new Stone(), 50));

        return new HousingBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withBaseCost(cost)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct farm builder.
     *
     * @return the builder
     */
    public Builder constructFarm(){
        int maxInhabitants = 5;
        int maxWorkers = 3;
        int constructionTime = 2;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.FARM;


        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 4));
        cost.add(new ResourceEntry(new Wood(), 5));
        cost.add(new ResourceEntry(new Stone(), 5));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Food(), 10));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct quarry builder.
     *
     * @return the builder
     */
    public Builder constructQuarry(){
        int maxInhabitants = 2;
        int maxWorkers = 30;
        int constructionTime = 2;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.QUARRY;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 4));
        cost.add(new ResourceEntry(new Wood(), 50));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Stone(), 4));
        production.add(new ResourceEntry(new Iron(), 4));
        production.add(new ResourceEntry(new Coal(), 4));
        production.add(new ResourceEntry(new Gold(), 2));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct lumber mill builder.
     *
     * @return the builder
     */
    public Builder constructLumberMill(){
        int maxInhabitants = 0;
        int maxWorkers = 10;
        int constructionTime = 4;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.LUMBER_MILL;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 6));
        cost.add(new ResourceEntry(new Wood(), 50));
        cost.add(new ResourceEntry(new Stone(), 50));

        List<ResourceEntry> consumption = new ArrayList<>();
        consumption.add(new ResourceEntry(new Wood(), 4));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Lumber(), 4));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withConsumption(consumption)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct cement plant builder.
     *
     * @return the builder
     */
    public Builder constructCementPlant(){
        int maxInhabitants = 0;
        int maxWorkers = 10;
        int constructionTime = 4;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.CEMENT_PLANT;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 6));
        cost.add(new ResourceEntry(new Wood(), 50));
        cost.add(new ResourceEntry(new Stone(), 50));

        List<ResourceEntry> consumption = new ArrayList<>();
        consumption.add(new ResourceEntry(new Stone(), 4));
        consumption.add(new ResourceEntry(new Coal(), 4));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Cement(), 4));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withConsumption(consumption)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }


    /**
     * Construct steel mill builder.
     *
     * @return the builder
     */
    public Builder constructSteelMill(){
        int maxInhabitants = 0;
        int maxWorkers = 40;
        int constructionTime = 6;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.STEEL_MILL;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 6));
        cost.add(new ResourceEntry(new Wood(), 100));
        cost.add(new ResourceEntry(new Stone(), 50));

        List<ResourceEntry> consumption = new ArrayList<>();
        consumption.add(new ResourceEntry(new Iron(), 4));
        consumption.add(new ResourceEntry(new Coal(), 2));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Steel(), 4));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withConsumption(consumption)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

    /**
     * Construct tool factory builder.
     *
     * @return the builder
     */
    public Builder constructToolFactory(){
        int maxInhabitants = 0;
        int maxWorkers = 12;
        int constructionTime = 8;
        BuildingLevel level = BuildingLevel.LEVEL_I;
        BuildingType type = BuildingType.TOOL_FACTORY;

        List<ResourceEntry> cost = new ArrayList<>();
        cost.add(new ResourceEntry(new Gold(), 8));
        cost.add(new ResourceEntry(new Wood(), 50));
        cost.add(new ResourceEntry(new Stone(), 50));

        List<ResourceEntry> consumption = new ArrayList<>();
        consumption.add(new ResourceEntry(new Steel(), 4));
        consumption.add(new ResourceEntry(new Coal(), 4));

        List<ResourceEntry> production = new ArrayList<>();
        production.add(new ResourceEntry(new Tools(), 4));

        return new OfficeBuilder()
                .withMaxInhabitants(maxInhabitants)
                .withMaxWorkers(maxWorkers)
                .withBaseCost(cost)
                .withConsumption(consumption)
                .withProduction(production)
                .withConstructionTime(constructionTime)
                .withLevel(level)
                .withConstructionState(0)
                .withType(type);
    }

}
