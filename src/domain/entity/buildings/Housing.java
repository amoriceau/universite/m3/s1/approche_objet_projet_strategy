package domain.entity.buildings;

import domain.entity.inhabitants.Inhabitant;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The type Housing.
 */
public class Housing extends Building{

    /**
     * Instantiates a new Housing.
     *
     * @param baseCost          the base cost
     * @param maxInhabitants    the max inhabitants
     * @param inhabitants       the inhabitants
     * @param constructionTime  the construction time
     * @param constructionState the construction state
     * @param level             the level
     * @param type              the type
     */
    public Housing(List<ResourceEntry> baseCost, int maxInhabitants, Set<Inhabitant> inhabitants, int constructionTime, int constructionState, BuildingLevel level, BuildingType type) {
        super(level, type, baseCost, constructionTime, constructionState, maxInhabitants, inhabitants);
    }

    public BuildingLevel getLevel() {
        return level;
    }

    public void setLevel(BuildingLevel level) {
        this.level = level;
    }

    public void setBaseCost(List<ResourceEntry> baseCost) {
        this.baseCost = baseCost;
    }

    public void setInhabitants(Set<Inhabitant> inhabitants) {
        this.inhabitants = inhabitants;
    }

    public void addInhabitant(Inhabitant inhabitant){
        this.inhabitants.add(inhabitant);
    }

    public int getConstructionTime() {
        return constructionTime;
    }

    public void setConstructionTime(int constructionTime) {
        this.constructionTime = constructionTime;
    }

    public int getConstructionState() {
        return constructionState;
    }

    public void setConstructionState(int constructionState) {
        this.constructionState = constructionState;
    }

    public String toString() {
        return "Housing@"+this.type +"{" +
                "\n\t level=" + level +
                ",\n\t baseCost=" + baseCost +
                ",\n\t constructionTime=" + constructionTime +
                ",\n\t constructionState=" + constructionState +
                ",\n\t inhabitants=" + inhabitants +
                ",\n\t maxInhabitants=" + maxInhabitants +
                "\n} \n";
    }

}
