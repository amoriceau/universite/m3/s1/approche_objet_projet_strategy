package domain.entity.buildings;

import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;

import java.util.List;
import java.util.Set;

/**
 * The type Housing builder.
 */
public class HousingBuilder implements Builder{
    /**
     * The Level.
     */
    BuildingLevel level;
    /**
     * The Type.
     */
    BuildingType type;
    /**
     * The Base cost.
     */
    List<ResourceEntry> baseCost;
    /**
     * The Inhabitants.
     */
    Set<Inhabitant> inhabitants;
    /**
     * The Max inhabitants.
     */
    int maxInhabitants;
    /**
     * The Construction time.
     */
    int constructionTime;
    /**
     * The Construction state.
     */
    int constructionState;



    @Override
    public Builder withMaxInhabitants(int maxInhabitants) {
        this.maxInhabitants = maxInhabitants;
        return this;
    }

    @Override
    public Builder withMaxWorkers(int maxWorkers) {
        return this;
    }

    @Override
    public Builder withConstructionTime(int constructionTime) {
        this.constructionTime = constructionTime;
        return this;
    }

    @Override
    public Builder withConstructionState(int constructionState) {
        this.constructionState = constructionState;
        return this;
    }

    @Override
    public Builder withInhabitants(Set<Inhabitant> inhabitants) {
        this.inhabitants = inhabitants;
        return this;
    }

    @Override
    public Builder withWorkers(Set<Worker> workers) {
        return this;
    }

    @Override
    public List<ResourceEntry> getBaseCost() {
        return this.baseCost;
    }

    @Override
    public Builder withBaseCost(List<ResourceEntry> baseCost) {
        this.baseCost = baseCost;
        return this;
    }

    @Override
    public Builder withProduction(List<ResourceEntry> production) {
        return this;
    }

    @Override
    public Builder withConsumption(List<ResourceEntry> consumption) {
        return this;
    }

    @Override
    public Builder withLevel(BuildingLevel level) {
        this.level = level;
        return this;
    }

    @Override
    public Builder withType(BuildingType type){
        this.type = type;
        return this;
    }

    public Housing build() {
        return new Housing(baseCost, maxInhabitants,inhabitants, constructionTime, constructionState, level, type);
    }
}
