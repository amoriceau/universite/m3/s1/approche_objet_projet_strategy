package domain.entity.buildings;

import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The type Office.
 */
public class Office extends Building {
    /**
     * The Production.
     */
    List<ResourceEntry> production;
    /**
     * The Consumption.
     */
    List<ResourceEntry> consumption;

    /**
     * The Max workers.
     */
    int maxWorkers;

    /**
     * The Workers.
     */
    Set<Worker> workers;

    /**
     * Instantiates a new Office.
     *
     * @param baseCost          the base cost
     * @param production        the production
     * @param consumption       the consumption
     * @param maxInhabitants    the max inhabitants
     * @param inhabitants       the inhabitants
     * @param maxWorkers        the max workers
     * @param workers           the workers
     * @param constructionTime  the construction time
     * @param constructionState the construction state
     * @param level             the level
     * @param type              the type
     */
    public Office(List<ResourceEntry> baseCost, List<ResourceEntry> production, List<ResourceEntry> consumption, int maxInhabitants, Set<Inhabitant> inhabitants, int maxWorkers, Set<Worker> workers, int constructionTime, int constructionState, BuildingLevel level, BuildingType type) {
        super(level, type, baseCost, constructionTime, constructionState, maxInhabitants, inhabitants);
        this.maxWorkers = maxWorkers;
        this.workers = new HashSet<>();
        this.production = new ArrayList<>();
        this.consumption = new ArrayList<>();

        if(workers != null){
            this.workers = workers;
        }

        if (production != null){
            this.production = production;
        }

        if (consumption != null){
            this.consumption = consumption;
        }

    }

    /**
     * Gets production.
     *
     * @return the production
     */
    public List<ResourceEntry> getProduction() {
        List<ResourceEntry> computedProduction = new ArrayList<>();
        for (ResourceEntry resource: production) {
            computedProduction.add(new ResourceEntry(resource.getResource(), resource.getQuantity() * level.getValue()));
        }
        return production;
    }

    /**
     * Sets production.
     *
     * @param production the production
     */
    public void setProduction(List<ResourceEntry> production) {
        this.production = production;
    }

    /**
     * Gets consumption.
     *
     * @return the consumption
     */
    public List<ResourceEntry> getConsumption() {
        List<ResourceEntry> computedConsumption = new ArrayList<>();
        for (ResourceEntry resource: consumption) {
            computedConsumption.add(new ResourceEntry(resource.getResource(), (resource.getQuantity() * level.getValue()) /2));
        }
        return computedConsumption;
    }

    /**
     * Sets consumption.
     *
     * @param consumption the consumption
     */
    public void setConsumption(List<ResourceEntry> consumption) {
        this.consumption = consumption;
    }

    /**
     * Gets max workers.
     *
     * @return the max workers
     */
    public int getMaxWorkers() {
        int ceil = switch (this.level){
            case LEVEL_I -> 0;
            case LEVEL_II -> Math.max(maxWorkers / 10, 1);
            case LEVEL_III -> Math.max((maxWorkers / 10)*2, 2);
            case LEVEL_IV -> Math.max((maxWorkers / 10) *4, 4);
            case LEVEL_V -> Math.max((maxWorkers / 10) * 5, 8);
        };
        return maxWorkers + ceil;
    }

    /**
     * Sets max workers.
     *
     * @param maxWorkers the max workers
     */
    public void setMaxWorkers(int maxWorkers) {
        this.maxWorkers = maxWorkers;
    }
    
    public void setInhabitants(Set<Inhabitant> inhabitants) {
        this.inhabitants = inhabitants;
    }

    /**
     * Sets workers.
     *
     * @param workers the workers
     */
    public void setWorkers(Set<Worker> workers) {
        this.workers = workers;
    }

    /**
     * Add worker.
     *
     * @param worker the worker
     */
    public void addWorker(Worker worker){
        this.workers.add(worker);
    }

    /**
     * Remove worker.
     *
     * @param worker the worker
     */
    public void removeWorker(Worker worker){
        this.workers.remove(worker);
    }

    /**
     * Get workers list.
     *
     * @return the list
     */
    public List<Worker> getWorkers(){
        List<Worker> w = new ArrayList<Worker>();
        w.addAll(workers);
        return w;
    }

    /**
     * Is full of workers boolean.
     *
     * @return the boolean
     */
    public boolean isFullOfWorkers(){
        return  this.workers.size() == this.getMaxWorkers();
    }

    public String toString() {
        return "Office@"+this.type +"{" +
                "\n\t level=" + level +
                ",\n\t baseCost=" + baseCost +
                ",\n\t constructionTime=" + constructionTime +
                ",\n\t constructionState=" + constructionState +
                ",\n\t inhabitants=" + inhabitants +
                ",\n\t maxInhabitants=" + maxInhabitants +
                ",\n\t workers=" + workers +
                ",\n\t maxWorkers=" + maxWorkers +
                ",\n\t production=" + production +
                ",\n\t consumption=" + consumption +
                "\n} \n";
    }

}
