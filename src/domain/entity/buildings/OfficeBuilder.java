package domain.entity.buildings;

import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;

import java.util.List;
import java.util.Set;

/**
 * The type Office builder.
 */
public class OfficeBuilder implements Builder{
    /**
     * The Level.
     */
    BuildingLevel level;
    /**
     * The Type.
     */
    BuildingType type;
    /**
     * The Base cost.
     */
    List<ResourceEntry> baseCost;
    /**
     * The Production.
     */
    List<ResourceEntry> production;
    /**
     * The Consumption.
     */
    List<ResourceEntry> consumption;
    /**
     * The Inhabitants.
     */
    Set<Inhabitant> inhabitants;
    /**
     * The Workers.
     */
    Set<Worker> workers;
    /**
     * The Max inhabitants.
     */
    int maxInhabitants;
    /**
     * The Max workers.
     */
    int maxWorkers;

    /**
     * The Construction time.
     */
    int constructionTime;
    /**
     * The Construction state.
     */
    int constructionState;



    @Override
    public Builder withMaxInhabitants(int maxInhabitants) {
        this.maxInhabitants = maxInhabitants;
        return this;
    }

    @Override
    public Builder withMaxWorkers(int maxWorkers) {
        this.maxWorkers = maxWorkers;
        return this;
    }

    @Override
    public Builder withConstructionTime(int constructionTime) {
        this.constructionTime = constructionTime;
        return this;
    }

    @Override
    public Builder withConstructionState(int constructionState) {
        this.constructionState = constructionState;
        return this;
    }

    @Override
    public Builder withInhabitants(Set<Inhabitant> inhabitants) {
        this.inhabitants = inhabitants;
        return this;
    }

    @Override
    public Builder withWorkers(Set<Worker> workers) {
        this.workers = workers;
        return this;
    }

    @Override
    public List<ResourceEntry> getBaseCost() {
        return this.baseCost;
    }

    @Override
    public Builder withBaseCost(List<ResourceEntry> baseCost) {
        this.baseCost = baseCost;
        return this;
    }

    @Override
    public Builder withProduction(List<ResourceEntry> production) {
        this.production = production;
        return this;
    }

    @Override
    public Builder withConsumption(List<ResourceEntry> consumption) {
        this.consumption = consumption;
        return this;
    }

    @Override
    public Builder withLevel(BuildingLevel level) {
        this.level = level;
        return this;
    }

    @Override
    public Builder withType(BuildingType type){
        this.type = type;
        return this;
    }

    public Office build() {
        return new Office(baseCost, production, consumption,maxInhabitants, inhabitants, maxWorkers, workers, constructionTime, constructionState, level, type);
    }
}
