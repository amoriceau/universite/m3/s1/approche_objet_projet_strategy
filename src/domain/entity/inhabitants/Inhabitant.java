package domain.entity.inhabitants;

import shared.abstracts.Unique;
import domain.entity.buildings.Building;
import shared.enums.InhabitantSex;

/**
 * The type Inhabitant.
 */
public class Inhabitant extends Unique {

    /**
     * The Age.
     */
    int age;
    /**
     * The Sex.
     */
    InhabitantSex sex;

    /**
     * The Living place.
     */
    Building livingPlace;
    /**
     * The Happiness.
     */
    int happiness = 5;

    /**
     * Instantiates a new Inhabitant.
     *
     * @param age          the age
     * @param sex          the sex
     * @param living_place the living place
     */
    public Inhabitant(int age, InhabitantSex sex, Building living_place) {
        this(age, sex);
        this.livingPlace = living_place;
    }

    /**
     * Instantiates a new Inhabitant.
     *
     * @param age the age
     * @param sex the sex
     */
    public Inhabitant(int age, InhabitantSex sex) {
        this.age = age;
        this.sex = sex;
    }

    /**
     * Gets age.
     *
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * Sets age.
     *
     * @param age the age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Gets sex.
     *
     * @return the sex
     */
    public InhabitantSex getSex() {
        return sex;
    }

    /**
     * Sets sex.
     *
     * @param sex the sex
     */
    public void setSex(InhabitantSex sex) {
        this.sex = sex;
    }

    /**
     * Gets happiness.
     *
     * @return the happiness
     */
    public int getHappiness() {
        return happiness;
    }

    /**
     * Sets happiness.
     *
     * @param happiness the happiness
     */
    public void setHappiness(int happiness) {
        this.happiness = happiness;
    }

    /**
     * Gets living place.
     *
     * @return the living place
     */
    public Building getLivingPlace() {
        return livingPlace;
    }

    /**
     * Sets living place.
     *
     * @param livingPlace the living place
     */
    public void setLivingPlace(Building livingPlace) {
        this.livingPlace = livingPlace;
    }

    /**
     * Promote worker.
     *
     * @return the worker
     */
    public Worker promote(){
        Worker newWorker = new Worker(this.getAge(), this.getSex());
        Building livingPlace = this.getLivingPlace();
        if (livingPlace != null){
            livingPlace.getInhabitants().remove(this);
            this.setLivingPlace(null);
            newWorker.setLivingPlace(livingPlace);
            livingPlace.addInhabitant(newWorker);
        }

        return newWorker;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Inhabitant(");
        sb.append(this.age + "YO, ");
        sb.append(this.sex + ", ");
        sb.append("happiness: " + this.happiness + ", ");
        sb.append((this.livingPlace != null ? "lives in:"+ (this.livingPlace.identity()): "homeless"));
        sb.append(")");
        return sb.toString();
    }
}
