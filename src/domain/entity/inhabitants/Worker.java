package domain.entity.inhabitants;

import domain.entity.buildings.Building;
import domain.entity.buildings.Office;
import shared.enums.InhabitantSex;

/**
 * The type Worker.
 */
public class Worker extends Inhabitant {

    /**
     * The Working place.
     */
    Office workingPlace;

    /**
     * Instantiates a new Worker.
     *
     * @param age          the age
     * @param sex          the sex
     * @param building     the building
     * @param workingPlace the working place
     */
    public Worker(int age, InhabitantSex sex, Building building, Office workingPlace) {
        super(age, sex, building);
        this.workingPlace = workingPlace;
    }

    /**
     * Instantiates a new Worker.
     *
     * @param age the age
     * @param sex the sex
     */
    public Worker(int age, InhabitantSex sex) {
        super(age, sex);
    }

    /**
     * Instantiates a new Worker.
     *
     * @param age          the age
     * @param sex          the sex
     * @param workingPlace the working place
     */
    public Worker(int age, InhabitantSex sex, Office workingPlace) {
        super(age, sex);
        this.workingPlace = workingPlace;
    }

    /**
     * Gets working place.
     *
     * @return the working place
     */
    public Office getWorkingPlace() {
        return workingPlace;
    }

    /**
     * Sets working place.
     *
     * @param workingPlace the working place
     */
    public void setWorkingPlace(Office workingPlace) {
        this.workingPlace = workingPlace;
    }

    /**
     * Demote inhabitant.
     *
     * @return the inhabitant
     */
    public Inhabitant demote(){
        Inhabitant newInhabitant = new Inhabitant(this.getAge(), this.getSex());
        Building livingPlace = this.getLivingPlace();
        if (livingPlace != null){
            livingPlace.getInhabitants().remove(this);
            this.setLivingPlace(null);
            newInhabitant.setLivingPlace(livingPlace);
            livingPlace.addInhabitant(this);
        }

        this.workingPlace.removeWorker(this);
        return newInhabitant;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Worker(");
        sb.append(this.age + "YO, ");
        sb.append(this.sex + ", ");
        sb.append("happiness: " + this.happiness + ", ");
        sb.append((this.livingPlace != null ? "lives in:"+ (this.livingPlace.identity()) : "homeless") + ", ");
        sb.append((this.workingPlace != null ? "works at:"+ (this.workingPlace.identity()) : "not working"));
        sb.append(")");
        return sb.toString();
    }
}
