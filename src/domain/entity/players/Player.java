package domain.entity.players;

import domain.aggregate.City;
import shared.abstracts.Unique;

/**
 * The type Player.
 */
public class Player extends Unique {
    private String name;

    private City city;
    private boolean wantsToStop;
    private int turns;

    /**
     * Instantiates a new Player.
     *
     * @param name the name
     * @param city the city
     */
    public Player(String name, City city) {
        this(name);
        this.city = city;
    }

    /**
     * Instantiates a new Player.
     *
     * @param name the name
     */
    public Player(String name){
        this.wantsToStop = false;
        this.name = name;
        this.turns = 0;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public City getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(City city) {
        this.city = city;
    }

    /**
     * Wants to stop boolean.
     *
     * @return the boolean
     */
    public boolean wantsToStop() {
        return wantsToStop;
    }

    /**
     * Sets wants to stop.
     *
     * @param wantsToStop the wants to stop
     */
    public void setWantsToStop(boolean wantsToStop) {
        this.wantsToStop = wantsToStop;
    }

    /**
     * Increase turn count.
     */
    public void increaseTurnCount() {
        this.turns++;
    }

    /**
     * Get turns int.
     *
     * @return the int
     */
    public int getTurns(){
        return this.turns;
    }
}
