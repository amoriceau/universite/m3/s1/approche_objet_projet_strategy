package domain.entity.resources;

import shared.abstracts.Unique;
import domain.vo.Resource;

/**
 * The type Resource entry.
 */
public class ResourceEntry extends Unique {
    /**
     * The Resource.
     */
    protected final Resource resource;
    /**
     * The Quantity.
     */
    protected int quantity;

    /**
     * Instantiates a new Resource entry.
     *
     * @param resource the resource
     */
    public ResourceEntry(Resource resource) {
        this(resource, 0);
    }

    /**
     * Instantiates a new Resource entry.
     *
     * @param resource the resource
     * @param quantity the quantity
     */
    public ResourceEntry(Resource resource, int quantity) {
        this.resource = resource;
        this.quantity = quantity;
    }

    /**
     * Gets resource.
     *
     * @return the resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * Gets quantity.
     *
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity the quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.resource.getName() + "(" +this.getQuantity() +")");
        return sb.toString();
    }
}
