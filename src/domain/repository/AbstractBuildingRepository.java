package domain.repository;

import domain.entity.buildings.Building;
import shared.interfaces.Repository;

import java.util.List;
import java.util.UUID;

/**
 * The type Abstract building repository.
 */
public abstract class AbstractBuildingRepository implements Repository<Building> {
    /**
     * Find by city id list.
     *
     * @param cityId the city id
     * @return the list
     */
    protected abstract List<Building> findByCityId(UUID cityId);
}
