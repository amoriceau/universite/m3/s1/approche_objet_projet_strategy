package domain.repository;

import domain.aggregate.City;
import shared.interfaces.Repository;

/**
 * The type Abstract city repository.
 */
public abstract class AbstractCityRepository implements Repository<City> {

}
