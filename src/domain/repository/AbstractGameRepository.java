package domain.repository;

import domain.aggregate.Game;
import shared.interfaces.Repository;

import java.util.List;
import java.util.UUID;

/**
 * The type Abstract game repository.
 */
public class AbstractGameRepository implements Repository<Game> {
    @Override
    public Game findById(UUID uuid) {
        return null;
    }

    @Override
    public void save(Game entity) {

    }

    @Override
    public void delete(UUID uuid) {

    }

    @Override
    public List<Game> findAll() {
        return null;
    }
}
