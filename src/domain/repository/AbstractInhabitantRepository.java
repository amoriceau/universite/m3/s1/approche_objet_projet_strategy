package domain.repository;

import domain.entity.inhabitants.Inhabitant;
import shared.interfaces.Repository;

import java.util.List;
import java.util.UUID;

/**
 * The type Abstract inhabitant repository.
 */
public abstract class AbstractInhabitantRepository implements Repository<Inhabitant> {
    /**
     * Find by building id list.
     *
     * @param buildingId the building id
     * @return the list
     */
    protected abstract List<Inhabitant> findByBuildingId(UUID buildingId);
}
