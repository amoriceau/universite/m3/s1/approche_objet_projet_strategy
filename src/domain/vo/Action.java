package domain.vo;

import shared.enums.BuildingType;
import shared.enums.PlayerAction;

/**
 * The type Action.
 */
public record Action(PlayerAction playerAction, Object target) {}
