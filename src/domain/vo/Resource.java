package domain.vo;

/**
 * The type Resource.
 */
public abstract class Resource {
    /**
     * The Name.
     */
    protected String name;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }
}
