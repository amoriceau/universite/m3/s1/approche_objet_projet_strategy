package domain.vo;

import domain.entity.players.Player;

import java.util.List;

/**
 * The type Turn.
 */
public record Turn(Player player, List<Action> actions) {}
