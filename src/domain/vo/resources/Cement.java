package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Cement.
 */
public class Cement extends Resource {
    /**
     * Instantiates a new Cement.
     */
    public Cement() {
        this.name = "Cement";
    }
}
