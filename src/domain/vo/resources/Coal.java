package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Coal.
 */
public class Coal extends Resource {
    /**
     * Instantiates a new Coal.
     */
    public Coal() {
        this.name = "Coal";
    }
}
