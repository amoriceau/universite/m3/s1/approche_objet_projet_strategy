package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Food.
 */
public class Food extends Resource {
    /**
     * Instantiates a new Food.
     */
    public Food() {
        this.name = "Food";
    }
}
