package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Gold.
 */
public class Gold extends Resource {

    /**
     * Instantiates a new Gold.
     */
    public Gold() {
        this.name = "Gold";
    }
}
