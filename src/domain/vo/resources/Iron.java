package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Iron.
 */
public class Iron extends Resource {
    /**
     * Instantiates a new Iron.
     */
    public Iron() {
        this.name = "Iron";
    }
}
