package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Lumber.
 */
public class Lumber extends Resource {
    /**
     * Instantiates a new Lumber.
     */
    public Lumber() {
        this.name = "Lumber";
    }
}
