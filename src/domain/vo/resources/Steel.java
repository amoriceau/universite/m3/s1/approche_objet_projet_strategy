package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Steel.
 */
public class Steel extends Resource {
    /**
     * Instantiates a new Steel.
     */
    public Steel() {
        this.name = "Steel";
    }
}
