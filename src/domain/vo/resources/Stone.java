package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Stone.
 */
public class Stone extends Resource {
    /**
     * Instantiates a new Stone.
     */
    public Stone() {
        this.name = "Stone";
    }
}
