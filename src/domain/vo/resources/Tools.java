package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Tools.
 */
public class Tools extends Resource {
    /**
     * Instantiates a new Tools.
     */
    public Tools() {
        this.name = "Tools";
    }
}
