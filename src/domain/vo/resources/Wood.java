package domain.vo.resources;

import domain.vo.Resource;

/**
 * The type Wood.
 */
public class Wood extends Resource {
    /**
     * Instantiates a new Wood.
     */
    public Wood() {
        this.name = "Wood";
    }
}
