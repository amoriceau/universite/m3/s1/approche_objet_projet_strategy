package infrastructure.repository;

import domain.entity.buildings.Building;
import domain.entity.buildings.BuildingDirector;
import domain.entity.buildings.Office;
import domain.repository.AbstractBuildingRepository;
import shared.enums.BuildingType;

import java.util.*;

/**
 * The type Building repository.
 */
public class BuildingRepository extends AbstractBuildingRepository {
    private final Map<UUID, Building> buildings = new HashMap<>();

    /**
     * The Director.
     */
    public BuildingDirector director = new BuildingDirector();

    @Override
    public Building findById(UUID id) {
        return null;
    }

    @Override
    public List<Building> findAll() {
        return new ArrayList<>(this.buildings.values());
    }

    /**
     * Find all finished list.
     *
     * @return the list
     */
    public List<Building> findAllFinished() {
        List<Building> buildings = new ArrayList<>();
        for (Building building : this.buildings.values()) {
            if (building.isFinished()) {
                buildings.add(building);
            }
        }
        return buildings;
    }

    /**
     * Find all finished and not full list.
     *
     * @return the list
     */
    public List<Building> findAllFinishedAndNotFull() {
        List<Building> buildings = new ArrayList<>();
        for (Building building : this.buildings.values()) {
            if (building.isFinished() && !building.isFullOfInhabitants()) {
                buildings.add(building);
            }
        }
        return buildings;
    }

    /**
     * Find all unfinished list.
     *
     * @return the list
     */
    public List<Building> findAllUnfinished() {
        List<Building> buildings = new ArrayList<>();
        for (Building building : this.buildings.values()) {
            if (!building.isFinished()) {
                buildings.add(building);
            }
        }
        return buildings;
    }

    /**
     * Find by building type list.
     *
     * @param types the types
     * @return the list
     */
    public List<Building> findByBuildingType(BuildingType... types) {
        List<Building> buildings = new ArrayList<>();
        for (BuildingType type : types) {
            for (Building building : this.buildings.values()) {
                if (building.getType().equals(type)) {
                    buildings.add(building);
                }
            }
        }

        return buildings;
    }

    /**
     * Find finished by building type list.
     *
     * @param types the types
     * @return the list
     */
    public List<Building> findFinishedByBuildingType(BuildingType... types) {
        List<Building> buildings = new ArrayList<>();
        for (BuildingType type : types) {
            for (Building building : this.findAllFinished()) {
                if (building.getType().equals(type)) {
                    buildings.add(building);
                }
            }
        }

        return buildings;
    }

    /**
     * Find all office list.
     *
     * @return the list
     */
    public List<Office> findAllOffice(){
        List<Office> offices = new ArrayList<>();
        List<Building> workingPlaces = this.findFinishedByBuildingType(BuildingType.FARM, BuildingType.QUARRY, BuildingType.CEMENT_PLANT, BuildingType.LUMBER_MILL, BuildingType.STEEL_MILL, BuildingType.TOOL_FACTORY, BuildingType.WOODEN_CABIN);
        for (Building workingPlace: workingPlaces) {
            offices.add((Office) workingPlace);
        }

        return offices;
    }

    /**
     * Find all finished office list.
     *
     * @return the list
     */
    public List<Office> findAllFinishedOffice(){
        List<Office> offices = new ArrayList<>();
        List<Building> workingPlaces = this.findFinishedByBuildingType(BuildingType.FARM, BuildingType.QUARRY, BuildingType.CEMENT_PLANT, BuildingType.LUMBER_MILL, BuildingType.STEEL_MILL, BuildingType.TOOL_FACTORY, BuildingType.WOODEN_CABIN);
        for (Building workingPlace: workingPlaces) {
            if(workingPlace.isFinished()){
                offices.add((Office) workingPlace);
            }
        }
        return offices;
    }


    @Override
    public List<Building> findByCityId(UUID cityId) {
        return null;
    }

    @Override
    public void save(Building building) {
        this.buildings.put(building.getUuid(), building);
    }

    @Override
    public void delete(UUID id) {
        this.buildings.remove(id);
    }

    @Override
    public String toString() {
        return "BuildingRepository{" +
                "buildings=" + buildings +
                '}';
    }
}
