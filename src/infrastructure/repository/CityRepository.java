package infrastructure.repository;

import domain.aggregate.City;
import domain.repository.AbstractCityRepository;

import java.util.List;
import java.util.UUID;

/**
 * The type City repository.
 */
public class CityRepository extends AbstractCityRepository {
    @Override
    public City findById(UUID uuid) {
        return null;
    }

    @Override
    public void save(City entity) {

    }

    @Override
    public void delete(UUID uuid) {

    }

    @Override
    public List<City> findAll() {
        return null;
    }
}
