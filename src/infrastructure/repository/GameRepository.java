package infrastructure.repository;

import domain.aggregate.City;
import domain.aggregate.Game;
import domain.repository.AbstractGameRepository;
import shared.enums.BuildingType;
import domain.entity.buildings.Building;
import domain.entity.buildings.Housing;
import domain.entity.buildings.Office;
import domain.entity.players.Player;
import domain.entity.resources.ResourceEntry;
import domain.entity.inhabitants.Inhabitant;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.UUID;

/**
 * The type Game repository.
 */
public class GameRepository extends AbstractGameRepository {
    @Override
    public Game findById(UUID uuid) {
        return null;
    }

    @Override
    public void save(Game entity){
        String nameDir = "saves/";
        File dir = new File(nameDir);
        if(!dir.exists()){
            dir.mkdir();
        }
        String fileName = nameDir + entity.getUuid().toString() + ".csv";
        File save = new File(fileName);
        if(save.isFile()){
            save.delete();
        }

        StringBuilder gameData = new StringBuilder();

        for (Player player : entity.getPlayers()) {
            /** Saving the player data **/
            gameData.append(player.getName());
            gameData.append("," + player.wantsToStop());
            gameData.append("," + player.getTurns() + "\n");

            /** Saving the buildings data **/ 
            City playerCity = player.getCity();
            int count = 0;
            for (Building building : playerCity.getBuildingRepository().findAll()) {
                if(count>0){
                    gameData.append(",");
                }
                gameData.append(building.getType().toString() + "," + building.getConstructionState() + "," + building.getLevel());
                if(building.getType().equals(BuildingType.HOUSE) || building.getType().equals(BuildingType.APARTMENT)){
                    Housing b = (Housing) building;
                    gameData.append("," + b.getInhabitants().size() + ",0");
                }else{
                    Office b = (Office) building;
                    gameData.append("," + b.getInhabitants().size() + "," + b.getWorkers().size());
                }
                count++;
            }
            count = 0;
            gameData.append("\n");
            /** Saving the inhabitants data **/
            for (Inhabitant inhabitant : playerCity.getInhabitantRepository().findAll()) {
                if(count>0){
                    gameData.append(",");
                }
                gameData.append(inhabitant.getSex().toString() + "," + inhabitant.getAge() + "," + inhabitant.getHappiness());
                count++;
            }
            count = 0;
            gameData.append("\n");

            /** Saving the resource data **/
            for (ResourceEntry entry : playerCity.getResources().values()) {
                if(count>0){
                    gameData.append(",");
                }
                gameData.append(entry.getResource().getName() + "," + entry.getQuantity());
                count++;
            }
            count = 0;
            gameData.append("\n");
        }

        try(FileWriter writer = new FileWriter(fileName)){


            writer.write(gameData.toString());
            


        } catch(Exception e) {
            System.err.println(e);
        }

    }

    @Override
    public void delete(UUID uuid) {

    }

    @Override
    public List<Game> findAll() {
        return null;
    }
}
