package infrastructure.repository;

import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.repository.AbstractInhabitantRepository;

import java.util.*;

/**
 * The type Inhabitant repository.
 */
public class InhabitantRepository extends AbstractInhabitantRepository {
    private final Map<UUID, Inhabitant> inhabitants = new HashMap<>();

    @Override
    public Inhabitant findById(UUID uuid) {
        return null;
    }

    @Override
    public List<Inhabitant> findAll() {
        return new ArrayList<>(this.inhabitants.values());
    }

    /**
     * Find all without living place list.
     *
     * @return the list
     */
    public List<Inhabitant> findAllWithoutLivingPlace() {
        List<Inhabitant> homeless = new ArrayList<>();
        for (Inhabitant inhabitant : inhabitants.values()) {
            if (inhabitant.getLivingPlace() == null) {
                homeless.add(inhabitant);
            }
        }
        return homeless;
    }

    /**
     * Find all without working place list.
     *
     * @return the list
     */
    public List<Worker> findAllWithoutWorkingPlace() {
        List<Worker> noWorkPlace = new ArrayList<>();
        for (Worker worker : this.findAllWorkers()) {
            if (worker.getWorkingPlace() == null) {
                noWorkPlace.add(worker);
            }
        }
        return noWorkPlace;
    }

    /**
     * Find all workers list.
     *
     * @return the list
     */
    public List<Worker> findAllWorkers() {
        List<Worker> workers = new ArrayList<>();
        for (Inhabitant inhabitant : inhabitants.values()) {
            if (inhabitant instanceof Worker) {
                workers.add((Worker) inhabitant);
            }
        }
        return workers;
    }

    @Override
    public List<Inhabitant> findByBuildingId(UUID buildingId) {
        return null;
    }

    @Override
    public void save(Inhabitant inhabitant) {
        this.inhabitants.put(inhabitant.getUuid(), inhabitant);

    }

    @Override
    public void delete(UUID id) {
        this.inhabitants.remove(id);
    }

    @Override
    public String toString() {
        return "InhabitantRepository{" +
                "inhabitants=" + inhabitants +
                "} ";
    }
}
