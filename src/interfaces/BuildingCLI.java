package interfaces;

import application.CityFacade;
import domain.aggregate.City;
import domain.entity.buildings.Building;
import domain.entity.resources.ResourceEntry;
import shared.enums.BuildingLevel;
import shared.enums.BuildingType;
import shared.utils.ANSICodes;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Building cli.
 */
public class BuildingCLI {
    /**
     * Get building cost string.
     *
     * @param city the city
     * @param type the type
     * @return the string
     */
    public static String getBuildingCost(City city, BuildingType type){
        List<ResourceEntry> cost = CityFacade.getBuildingCost(city, type);
        return BuildingCLI.buildingCostString(city, cost);
    }

    /**
     * Get building cost at level string.
     *
     * @param city  the city
     * @param type  the type
     * @param level the level
     * @return the string
     */
    public static String getBuildingCostAtLevel(City city, BuildingType type, BuildingLevel level){
        List<ResourceEntry> cost = CityFacade.getBuildingCostAtLevel(city, type, level);
        return BuildingCLI.buildingCostString(city, cost);
    }

    private static String buildingCostString(City city, List<ResourceEntry> cost){
        StringBuilder costString = new StringBuilder();
        int cpt = 0;
        for (ResourceEntry resource: cost) {
            List<ResourceEntry> resourceList = new ArrayList<>();
            resourceList.add(resource);
            if (cpt>0){
                costString.append(" | ");
            }
            if (!city.hasResources(false,resourceList)){
                costString.append(ANSICodes.RED + resource + ANSICodes.RESET);
            }else {
                costString.append(ANSICodes.GREEN + resource + ANSICodes.RESET);
            }
            cpt++;
        }
        return " (Cost: " + costString + ")" + ANSICodes.RESET;
    }
}
