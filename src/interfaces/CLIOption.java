package interfaces;

/**
 * The type Cli option.
 */
public record CLIOption(String value, String description, Runnable action) { }
