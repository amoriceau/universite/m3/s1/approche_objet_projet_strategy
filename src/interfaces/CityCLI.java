package interfaces;

import domain.aggregate.City;
import domain.entity.buildings.Building;
import domain.entity.buildings.Office;
import domain.entity.inhabitants.Inhabitant;
import domain.entity.inhabitants.Worker;
import domain.entity.resources.ResourceEntry;
import shared.utils.ANSICodes;

/**
 * The type City cli.
 */
public class CityCLI {
    /**
     * Display resources table.
     *
     * @param city the city
     */
    public static void displayResourcesTable(City city){
        int columnSize = 50;
        Runnable printBorder = () -> {
            System.out.println("+" + "-".repeat(columnSize) + "+" + "-".repeat(columnSize) + "+");
        };

        System.out.println(ANSICodes.BOLD + ANSICodes.UNDERLINE + "RESOURCES" + ANSICodes.RESET);
        printBorder.run();
        System.out.printf("| %-48s | %48s |%n", "Name", "Quantity");
        printBorder.run();

        for (ResourceEntry resource : city.getResources().values()) {
            String prefix = ANSICodes.BOLD;
            if (resource.getQuantity() == 0){
                prefix +=ANSICodes.RED;
            } else if (resource.getQuantity() < 10) {
                prefix +=ANSICodes.YELLOW;
            }else{
                prefix+=ANSICodes.GREEN;
            }

            System.out.printf("| %-61s | %61s |%n",
                    prefix +resource.getResource().getName() + ANSICodes.RESET,
                    prefix + resource.getQuantity() + ANSICodes.RESET);
        }
        printBorder.run();
        System.out.println("");
    }

    /**
     * Display inhabitants table.
     *
     * @param city the city
     */
    public static void displayInhabitantsTable(City city){
        System.out.println(ANSICodes.BOLD + ANSICodes.UNDERLINE +"INHABITANTS" + ANSICodes.RESET+ "(" + city.getInhabitantRepository().findAll().size() + " inhabitants whose "+ city.getInhabitantRepository().findAllWorkers().size() +" are workers)");
        int columnSize = 143;
        Runnable printBorder = () -> {
            System.out.println("+" + "-".repeat(columnSize) + "+");
        };
        printBorder.run();
        System.out.printf("| %-36s | %6s | %4s | %30s | %7s | %30s | %10s |%n", "UUID", "Gender", "Age", "Living Place", "Worker?", "Working at", "Happiness");
        printBorder.run();

        for (Inhabitant inhabitant : city.getInhabitantRepository().findAll()) {
            boolean isWorker = inhabitant instanceof Worker;
            String livingPlaceDisplay = inhabitant.getLivingPlace() == null ? "homeless" : (inhabitant.getLivingPlace().identity());
            String workingPlaceDisplay =  "NA";

            if (isWorker){
                workingPlaceDisplay = ((Worker)inhabitant).getWorkingPlace() != null ? ((Worker) inhabitant).getWorkingPlace().identity() : "None";
            }

            System.out.printf("| %-36s | %6s | %4d | %30s | %7b | %30s | %10s |%n",
                    ANSICodes.BOLD + ANSICodes.ITALIC + inhabitant.getUuid() + ANSICodes.RESET,
                    inhabitant.getSex().toString(),
                    inhabitant.getAge(),
                    livingPlaceDisplay,
                    isWorker,
                    workingPlaceDisplay,
                    inhabitant.getHappiness());
        }
        printBorder.run();
        System.out.println("");
    }

    /**
     * Display buildings table.
     *
     * @param city the city
     */
    public static void displayBuildingsTable(City city){
        System.out.println(ANSICodes.BOLD + ANSICodes.UNDERLINE + "BUILDINGS"+ ANSICodes.RESET+ "(" + city.getBuildingRepository().findAll().size() + ")");
        int columnSize = 114;
        Runnable printBorder = () -> {
            System.out.println("+" + "-".repeat(columnSize) + "+");
        };

        printBorder.run();
        System.out.printf("| %-36s | %20s | %12s | %12s | %9s | %8s |%n", "UUID", "Type", "Ready", "Inhabitants", "Workers", "Level");
        printBorder.run();


        // Print each row
        for (Building building : city.getBuildingRepository().findAll()) {
            String workers = building instanceof Office ? ((Office) building).getWorkers().size() + "/" + ((Office) building).getMaxWorkers() : "NA";
            String inhabitants = (building.getInhabitants().size() == building.getMaxInhabitants() ? "(full) " : "") + building.getInhabitants().size()+"/" + building.getMaxInhabitants();
            String buildingStatus = building.isFinished() +" (" + (building.getConstructionState() +"/" + building.getConstructionTime()) + ")";
            System.out.printf("| %-36s | %20s | %12s | %12s | %9s | %8s |%n",
                    ANSICodes.BOLD + ANSICodes.ITALIC + building.getUuid() + ANSICodes.RESET,
                    building.getType (),
                    buildingStatus,
                    inhabitants,
                    workers,
                    building.getLevel()
            );
        }

        // Print table bottom border
        printBorder.run();
        System.out.println("");
    }

    /**
     * Display advice.
     *
     * @param city the city
     */
    public static void displayAdvice(City city){
        for (ResourceEntry resource: city.getResources().values()) {
            if (resource.getQuantity() < 5){
                System.out.println(ANSICodes.BOLD+ ANSICodes.ITALIC + ANSICodes.RED + "/!\\ Your " + resource.getResource().getName() + " supply is critical." + ANSICodes.RESET);
            }else if (resource.getQuantity() < 10){
                System.out.println(ANSICodes.BOLD+ ANSICodes.ITALIC + ANSICodes.YELLOW + "Your " + resource.getResource().getName() + " supply is low." + ANSICodes.RESET);
            }
        }
        boolean isCityFull = true;
        for (Building building: city.getBuildingRepository().findAll()) {
            if (!building.isFullOfInhabitants()){
                isCityFull = false;
            }
        }
        if (isCityFull) System.out.println(ANSICodes.BOLD+ ANSICodes.ITALIC + ANSICodes.CYAN + "You should build new Houses or Apartment so new citizens can move in!\n" + ANSICodes.RESET);
    }

}
