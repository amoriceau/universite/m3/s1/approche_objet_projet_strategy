package interfaces;

import application.services.GameService;
import domain.aggregate.City;
import domain.entity.buildings.Building;
import domain.entity.players.Player;
import domain.vo.Action;
import shared.enums.BuildingType;
import shared.enums.PlayerAction;
import shared.utils.ANSICodes;
import shared.utils.Sleep;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The type Game cli.
 */
public class GameCLI {
    private final GameService gameService;
    private Scanner scanner;


    /**
     * Instantiates a new Game cli.
     *
     * @param gameService the game service
     */
    public GameCLI(GameService gameService) {
        this.gameService = gameService;
        this.scanner = new Scanner(System.in);
    }

    /**
     * Start game session.
     */
    public void startGameSession() {
        this.gameService.startGame();
    }

    /**
     * Display turn options.
     *
     * @param options the options
     */
    public void displayTurnOptions(CLIOption[] options) {
        for (CLIOption option : options) {
            System.out.println(option.value() + ": " + option.description());
        }
    }

    /**
     * Ask player and perform action.
     *
     * @param options the options
     */
    public void askPlayerAndPerformAction(CLIOption[] options) {
        String input;
        while (true) {
            System.out.println("Choose an option:");
            input = this.scanner.nextLine();

            CLIOption selectedOption = getOptionByValue(input, options);
            if (selectedOption != null) {
                selectedOption.action().run();
                break;
            }
            System.out.println("Invalid option, please try again.");
        }
    }

    private CLIOption getOptionByValue(String input, CLIOption[] options) {
        for (CLIOption option : options) {
            if (option.value().equalsIgnoreCase(input)) {
                return option;
            }
        }
        return null;
    }

    /**
     * Process player turn.
     */
    public void processPlayerTurn() {
        this.displayUserStatistics();
        CLIOption[] options = this.generateAvailablePlayerActions();
        this.displayTurnOptions(options);
        this.askPlayerAndPerformAction(options);
    }

    private void handlePlayerAction(PlayerAction action){
        CLIOption[] options = this.generateAvailableBuildingOptions(action);
        switch (action){
            case BUILD -> {
                System.out.println("Build a new building\n");
                this.displayTurnOptions(options);
                this.askPlayerAndPerformAction(options);
            }
            case DESTROY -> {
                System.out.println("Destroy a building\n");
                CLIOption[] destructionOptions = this.generateBuildingsToDestroy();
                this.displayTurnOptions(destructionOptions);
                this.askPlayerAndPerformAction(destructionOptions);
            }
            case UPGRADE -> {
                CLIOption[] buildingsToUpgrade = this.generateBuildingsToUpgrade();
                System.out.println("Upgrade a building\n");
                this.displayTurnOptions(buildingsToUpgrade);
                this.askPlayerAndPerformAction(buildingsToUpgrade);
            }
            case END_TURN -> {
                System.out.println("Processing the end turn phase.");
                this.gameService.handleCurrentPlayerTurn();
                new Sleep(1500);
            }
            case SAVE -> {
                System.out.println("Save the game");
                this.gameService.saveGame();
            }
            case LOAD -> {
                System.out.println("Load a game");
            }
            case END_GAME -> {
                System.out.println("End game");
                this.gameService.currentPlayerWantsToStop();
                this.gameService.handleCurrentPlayerTurn();
            }
        }
        System.out.println("\n\n");
        System.out.println(ANSICodes.BOLD + ANSICodes.YELLOW + "#".repeat(170) + ANSICodes.RESET);
        System.out.println(ANSICodes.BOLD + ANSICodes.YELLOW + "#".repeat(170) + ANSICodes.RESET);
        System.out.println(ANSICodes.BOLD + ANSICodes.YELLOW + "#".repeat(170) + ANSICodes.RESET);
        System.out.println("\n\n");

    }

    private CLIOption[] generateAvailableBuildingOptions(PlayerAction action){
        List<CLIOption> options = new ArrayList<>();
        int counter = 1;
        City city = this.gameService.instance().getCurrentPlayer().getCity();
        for (BuildingType type : BuildingType.values()) {
            String prefix = ANSICodes.BOLD + ANSICodes.UNDERLINE;
            String desc = prefix +  action + " " + type + ANSICodes.RESET;
            desc += BuildingCLI.getBuildingCost(city, type);
            options.add(new CLIOption(String.valueOf(counter), desc, () -> this.gameService.performPlayerAction(new Action(action, type))));
            counter++;
        }
        options.add(new CLIOption(String.valueOf(counter), "Go back", () -> {}));


        return options.toArray(new CLIOption[0]);
    }

    private CLIOption[] generateBuildingsToUpgrade(){
        List<CLIOption> options = new ArrayList<>();
        List<Building> buildings = this.gameService.instance().getCurrentPlayer().getCity().getBuildingRepository().findAllFinished();
        City city = this.gameService.instance().getCurrentPlayer().getCity();

        int counter = 1;
        for (Building building : buildings) {
            String prefix = ANSICodes.BOLD + ANSICodes.UNDERLINE;
            String desc = prefix +  building.identity() + ANSICodes.RESET;
            desc += BuildingCLI.getBuildingCostAtLevel(city, building.getType(), building.getNextLevel());
            options.add(new CLIOption(String.valueOf(counter), desc, () -> this.gameService.performPlayerAction(new Action(PlayerAction.UPGRADE, building))));

            counter++;
        }
        options.add(new CLIOption(String.valueOf(counter), "Go back", () -> {}));


        return options.toArray(new CLIOption[0]);
    }

    private CLIOption[] generateBuildingsToDestroy(){
        List<CLIOption> options = new ArrayList<>();
        List<Building> buildings = this.gameService.instance().getCurrentPlayer().getCity().getBuildingRepository().findAll();

        int counter = 1;
        for (Building building : buildings) {
            String prefix = ANSICodes.BOLD + ANSICodes.UNDERLINE;
            String desc = prefix +  building.identity() + ANSICodes.RESET;
            desc += " " + ANSICodes.CYAN + building.getUuid() + (building.isFullOfInhabitants() ? (ANSICodes.RED +" (full)"): "") + ANSICodes.RESET;
            options.add(new CLIOption(String.valueOf(counter), desc, () -> this.gameService.performPlayerAction(new Action(PlayerAction.DESTROY, building))));

            counter++;
        }
        options.add(new CLIOption(String.valueOf(counter), "Go back", () -> {}));


        return options.toArray(new CLIOption[0]);
    }

    private CLIOption[] generateAvailablePlayerActions(){
        List<CLIOption> options = new ArrayList<>();
        int counter = 1;
        for (PlayerAction action : PlayerAction.values()) {
            options.add(new CLIOption(String.valueOf(counter), action.toString(), () -> handlePlayerAction(action)));
            counter++;
        }

        return options.toArray(new CLIOption[0]);
    }

    private void displayUserStatistics(){
        Player currentPlayer = this.gameService.instance().getCurrentPlayer();
        PlayerCLI.displayStatistics(currentPlayer);
        CityCLI.displayResourcesTable(currentPlayer.getCity());
        CityCLI.displayInhabitantsTable(currentPlayer.getCity());
        CityCLI.displayBuildingsTable(currentPlayer.getCity());
        CityCLI.displayAdvice(currentPlayer.getCity());
    }
}
