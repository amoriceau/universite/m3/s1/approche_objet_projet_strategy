package interfaces;

import domain.entity.players.Player;
import shared.utils.ANSICodes;

public class PlayerCLI {

    public static void displayStatistics(Player player) {
        System.out.println(ANSICodes.BOLD +"PLAYER: " +ANSICodes.GREEN + player.getName() + ANSICodes.RESET + " | Turn: " + ANSICodes.YELLOW + ANSICodes.BOLD + (player.getTurns() + 1) + ANSICodes.RESET +"\n");
    }
}
