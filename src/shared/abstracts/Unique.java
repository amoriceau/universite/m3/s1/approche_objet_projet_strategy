package shared.abstracts;

import java.util.UUID;

/**
 * The type Unique.
 */
public abstract class Unique {
    private final UUID uuid;

    /**
     * Instantiates a new Unique.
     */
    public Unique() {
        this.uuid = UUID.randomUUID();
    }

    /**
     * Gets uuid.
     *
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }
}
