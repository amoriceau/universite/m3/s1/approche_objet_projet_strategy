package shared.enums;

/**
 * The enum Building level.
 */
public enum BuildingLevel {
    /**
     * Level i building level.
     */
    LEVEL_I(1),
    /**
     * Level ii building level.
     */
    LEVEL_II(2),
    /**
     * Level iii building level.
     */
    LEVEL_III(3),
    /**
     * Level iv building level.
     */
    LEVEL_IV(4),
    /**
     * Level v building level.
     */
    LEVEL_V(5);

    private final int value;
    private BuildingLevel(int value) {
        this.value = value;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return this.value;
    }

}
