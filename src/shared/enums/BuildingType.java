package shared.enums;

/**
 * The enum Building type.
 */
public enum BuildingType {
    /**
     * House building type.
     */
    HOUSE,
    /**
     * Apartment building type.
     */
    APARTMENT,
    /**
     * Farm building type.
     */
    FARM,
    /**
     * Quarry building type.
     */
    QUARRY,
    /**
     * Cement plant building type.
     */
    CEMENT_PLANT,
    /**
     * Steel mill building type.
     */
    STEEL_MILL,
    /**
     * Lumber mill building type.
     */
    LUMBER_MILL,
    /**
     * Tool factory building type.
     */
    TOOL_FACTORY,

    /**
     * Wooden cabin building type.
     */
    WOODEN_CABIN,
}
