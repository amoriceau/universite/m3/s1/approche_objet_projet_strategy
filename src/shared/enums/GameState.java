package shared.enums;

/**
 * The enum Game state.
 */
public enum GameState {
    /**
     * Initializing game state.
     */
    INITIALIZING,
    /**
     * Paused game state.
     */
    PAUSED,
    /**
     * Running game state.
     */
    RUNNING,
    /**
     * Finished game state.
     */
    FINISHED
}
