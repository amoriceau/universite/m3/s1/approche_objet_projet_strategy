package shared.enums;

/**
 * The enum Inhabitant sex.
 */
public enum InhabitantSex {
    /**
     * M inhabitant sex.
     */
    M,
    /**
     * F inhabitant sex.
     */
    F;
}
