package shared.enums;

/**
 * The enum Player action.
 */
public enum PlayerAction {
    /**
     * Build player action.
     */
    BUILD,
    /**
     * Destroy player action.
     */
    DESTROY,

    /**
     * Upgrade player action.
     */
    UPGRADE,

    /**
     * Save player action.
     */
    SAVE,
    /**
     * Load player action.
     */
    LOAD,
    /**
     * End turn player action.
     */
    END_TURN,
    /**
     * End game player action.
     */
    END_GAME
}
