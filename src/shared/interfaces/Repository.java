package shared.interfaces;

import java.util.List;
import java.util.UUID;

/**
 * The interface Repository.
 *
 * @param <T> the type parameter
 */
public interface Repository<T> {
    /**
     * Find by id t.
     *
     * @param uuid the uuid
     * @return the t
     */
    T findById(UUID uuid);

    /**
     * Save.
     *
     * @param entity the entity
     */
    void save(T entity);

    /**
     * Delete.
     *
     * @param uuid the uuid
     */
    void delete(UUID uuid);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<T> findAll();
}
