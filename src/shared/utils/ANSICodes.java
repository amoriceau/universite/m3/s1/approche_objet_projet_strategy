package shared.utils;

/**
 * The type Ansi codes.
 */
public class ANSICodes {
    /**
     * The constant RESET.
     */
    public static final String RESET = "\u001B[0m";
    /**
     * The constant BLACK.
     */
    public static final String BLACK = "\u001B[30m";
    /**
     * The constant RED.
     */
    public static final String RED = "\u001B[31m";
    /**
     * The constant GREEN.
     */
    public static final String GREEN = "\u001B[32m";
    /**
     * The constant YELLOW.
     */
    public static final String YELLOW = "\u001B[33m";
    /**
     * The constant BLUE.
     */
    public static final String BLUE = "\u001B[34m";
    /**
     * The constant PURPLE.
     */
    public static final String PURPLE = "\u001B[35m";
    /**
     * The constant CYAN.
     */
    public static final String CYAN = "\u001B[36m";
    /**
     * The constant WHITE.
     */
    public static final String WHITE = "\u001B[37m";
    /**
     * The constant BOLD.
     */
    public static final String BOLD = "\u001B[1m";
    /**
     * The constant UNDERLINE.
     */
    public static final String UNDERLINE = "\u001B[4m";
    /**
     * The constant ITALIC.
     */
    public static final String ITALIC = "\u001B[3m";
    
}
