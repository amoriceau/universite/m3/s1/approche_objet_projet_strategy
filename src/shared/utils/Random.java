package shared.utils;

/**
 * The type Random.
 */
public class Random {

    /**
     * Between int.
     *
     * @param min the min
     * @param max the max
     * @return the int
     */
    public static int between(int min, int max){
        int range = (max - min) + 1;
        return  (int) ((range * Math.random()) + min);
    }

    /**
     * Value double.
     *
     * @return the double
     */
    public static double value(){
        return Math.random();
    }
}
