package shared.utils;

/**
 * The type Sleep.
 */
public class Sleep {
    /**
     * Instantiates a new Sleep.
     *
     * @param milliseconds the milliseconds
     */
    public Sleep(int milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            System.err.println("Interruption: " + e.getMessage());
        }
    }
}
